'use strict';

import Backbone from 'backbone';
import Marionette from 'backbone.marionette';

// Marionette inspector
if (window.__agent) {
  window.__agent.start(Backbone, Marionette);
}

import Application from './core/Application';

import {AddonCollection} from './models/AddonModel';

/** @type {Application} **/
let app = window.App = new Application();

app
  .loadCollections([AddonCollection])
  .then(app.fetchCurrentUser)
  .then(app.fetchCurrentAccount)
  .then(function() {
    app.start({ el: 'body' });
  })
  .catch(function(err) {
    document.body.innerHTML = 'Application start failed. Have you run npm run migrate?'
  });
