'use strict';

const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
  'accessToken': String,
  'accountId': { type: mongoose.Schema.Types.ObjectId, ref: 'Account' },
  'createdTime': { type: Date, default: Date.now() },
  'email': String,
  'expirationTime': { type: Number, default: -1 },
  'failedLogins': Array,
  'firstName': String,
  'history': Array,
  'lastLoginTime': Date,
  'lastName': String,
  'lastPasswordChange': Date,
  'locale': String,
  'password': String,
  'role': String,
  'settings': {
    type: Object,
    default: {
      'emails': {
        'newTicket': false,
        'sendAssignNotification': 'always'
      },
      'general': {
        'showAllTicketTab': false
      },
      'sidebarRight': {
        'commentBarrier': 500,
        'showUserFeedFirst': true
      }
    }
  },
  'updatedTime': Date,
  'usedOneTimeTokens': Array,
  'userName': String,
  'userVerifyOneTimeToken': String
});

module.exports = mongoose.model('User', UserSchema);
