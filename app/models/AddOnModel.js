'use strict';

const mongoose = require('mongoose');

/**
 * A valid accessor only consists of
 * lower and uppercase letters and a hyphen
 * or underscore
 */
const validAccessor = function(val) {
  return /^[A-Za-z_-]+$/.test(val)
}

const AddonSchema = mongoose.Schema({
  deleted: { type: Boolean, default: false },
  description: String,
  accessor: { type: String, required: true, unique: true, validate: validAccessor },
  image: String,
  title: { type: String, required: true },
  isPremium: { type: Boolean, default: false },
  notifyUsers: {type: Boolean, default: false }
});

module.exports = mongoose.model('Addon', AddonSchema);
