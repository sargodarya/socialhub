'use strict';

/**
 * Validates a given port number and warns if trying
 * to run on a privileged port.
 *
 * @param {number} port - port to be validated
 * @returns {number}    - validated port or null
 */
function parsePort(portToValidate) {
  const port = parseInt(portToValidate, 10);
  if (isNaN(port)) {
    return null;
  }

  // Check for privileged ports
  if (port > 0) {
    return port;
  }

  return null;
}

module.exports = parsePort;
