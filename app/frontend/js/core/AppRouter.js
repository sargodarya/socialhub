'use strict';

import * as Marionette from 'backbone.marionette';

/**
 * NOTE: Normally I would split this up into different
 * routers and controllers for example SettingsRouter and
 * SettingsController so we have a clear separation of concerns
 * but that doesn't make sense for this small sample.
 */
export default class AppRouter extends Marionette.AppRouter {
  constructor(...rest) {
    super(...rest);
  }
}