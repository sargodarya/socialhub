import Marionette from 'backbone.marionette';
import _ from 'lodash';

const AddonNoItemView = Marionette.ItemView.extend({
  className: 'addon__no-items',
  template: require('./templates/AddonNoItemView.jade'),
});

export default AddonNoItemView;