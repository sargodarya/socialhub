# Project Structure

## App Overview (Backend)

    +--- app
    |   +--- controllers
    |   +--- frontend
    |   |   +--- ... (see App Overview (Frontend))
    |   +--- lib
    |   +--- models
    |   +--- views
    +--- config
    |   +--- config.js
    |   +--- params.js
    |   +--- routes.js
    |   +--- webpack.config.js
    +--- dump
    +--- public
    +--- tasks

### /app

Contains all application specific code.

#### /app/controllers

Controller are defined in here. Every method defined in a controller
is available in the routes to be called with 'controllerName#methodName'.

#### /app/frontend

Contains all frontend related code which is built using webpack.

#### /app/lib

Holds all library code like DBConnection or ApplicationServer which
is used to bootstrap the server.

#### /app/models

All available application models are defined in here.

#### /app/views

The layout and view files which get rendered by the controller
are defined in here.

### /config

Everything which can be configured is sitting in this directory.

#### /config/config.js

Environment specific configuration like database and ports are
defined in this file.

#### /config/params.js

This is currently a hard coded list of express param transformation
but it might as well be automated at a later point.

#### /config/routes.js

Contains route mappings to controllers in the following form:

    'httpVerb /path/to/something': 'controllerName#methodName'