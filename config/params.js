/**
 * This file declares all params which might
 * be given into express and as such can be
 * automatically transformed into a representative
 * mongoose model.
 *
 * For resourceful routes this might be automated
 * at some later point.
 */

'use strict';

module.exports = function(app) {

  // This is straight up copied from here because
  // I wasn't sure about the syntax anymore
  // http://expressjs.com/api.html#app.param
  app.param('user', function(req, res, next, id) {
    User.findOne(id, function(err, user) {
      if (err) {
        next(err);
      } else if (user) {
        req.user = user;
        next();
      } else {
        next(new Error('failed to load user'));
      }
    });
  });

  app.param('account', function(req, res, next, id) {
    Account.findOne(id, function(err, account) {
      if (err) {
        next(err);
      } else if (account) {
        req.account = account;
        next();
      } else {
        next(new Error('failed to load account'));
      }
    });
  });

  app.param('addon', function(req, res, next, accessor) {
    Addon.findOne({ 'accessor': accessor }, function(err, addon) {
      if (err) {
        next(err);
      } else if (addon) {
        req.addon = addon;
        next();
      } else {
        next(new Error('failed to load addon'));
      }
    });
  });
}
