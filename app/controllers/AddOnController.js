'use strict';

class AddonController {

  static index(req, res) {
    Addon.find({}, function(err, addons) {
      switch(req.accepts('html', 'json')) {
        case 'html':
          res.render('addon/index', {
            addons: addons
          });
          break;

        case 'json':
          res.send(addons);
          break;
      }
      res.end();
    });
  }

  static show(req, res) {
    switch(req.accepts('html', 'json')) {
      case 'html':
        res.render('addon/show', {
          addon: req.addon
        })
        break;

      case 'json':
        res.send(req.addon);
        break;
    }

    res.end();
  }

  static create(req, res) {
    /**
     * Saves the created addon
     * @param {string} [imageName] - name of the stored image
     * @returns {void}
     */
    function persistAddon(imageName) {
      if (imageName) {
        req.body.addon.image = imageName;
      }

      Addon.create(req.body.addon, (err, addon) => {
        if (err) {
          if (req.is('json')) {
            res.status(400).send(err);
          } else if (req.is('html')) {
            res.status(400).render('addon/new', {
              addon: req.body.addon
            })
          }
          return res.end();
        }

        res.redirect(201, '/addon/' + addon.accessor);
        return res.end();
      });
    }

    // Check if we have a file
    if (req.file) {
      req.app
        .moveUploadedFile(req.file, req.app.config.root + '/public/uploads', req.body.addon.accessor)
        .then(persistAddon)
        .catch((err) => {
          res
            .status(500)
            .send(err)
            .end();
        });
    } else {
      persistAddon();
    }
  }

  static new(req, res) {
    res.render('addon/new');
    res.end();
  }

  static destroy(req, res) {
    let addon = req.params.addon;
    addon.deleted =
    req.params.addon
    res.end();
  }

  static edit(req, res) {
    res.render('addon/edit');
    res.end();
  }

  static update(req, res) {
    res.end();
  }
}

module.exports = AddonController;
