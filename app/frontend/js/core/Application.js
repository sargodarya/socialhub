'use strict';

import Backbone from 'backbone';
import Marionette from 'backbone.marionette';
import _ from 'lodash';

// Main Layout files
import RootLayout from '../views/layouts/RootLayout';

import HeaderView from '../views/header/HeaderView';

import ApplicationController from './ApplicationController';

import AppRouter from './AppRouter';

import {AccountModel} from '../models/AccountModel';
import {AddonModel, AddonCollection} from '../models/AddonModel';
import {UserModel, UserCollection} from '../models/UserModel';

/**
 * Contains our base application used to bootstrap
 * everything
 */
export default class Application extends Marionette.Application {
  constructor(...rest) {
    super(...rest);

    /** @type {RootLayout} **/
    this.rootLayout = null;

    /** @type {Object} **/
    this.collections = {};

    /** @type {ApplicationController} **/
    this.Controllers = {
      ApplicationController: new ApplicationController(this)
    }

    /** @type {AppRouter} **/
    this.router = new AppRouter({
      controller: this.Controllers.ApplicationController,
      appRoutes: ApplicationController.routes()
    });

    /** @type {Region} **/
    this.contentArea = null;

    /** @type {UserModel} **/
    this.currentUser = new UserModel();

    /** @type {UserModel} **/
    this.currentAccount = new AccountModel();

    this.bindEventHandlers();

    // Bind fetch methods so chained promises
    // dont have problems with the right context
    _.bindAll(this, 'fetchCurrentUser', 'fetchCurrentAccount');
  }

  /**
   * Loads given collections into memory
   * @param {Array<Backbone.Collection>} collections
   * @returns {Promise}
   */
  loadCollections(collections) {
    let promises = collections.map(collection => {
      return new Promise((resolve, reject) => {
        let collectionInstance = this.collections[collection.collectionName] = new collection();
        collectionInstance.fetch({
          success: (collection) => {
            resolve();
          },
          error: (collection, response, options) => {
            reject(collection, response, options);
          }
        });
      });
    });

    return Promise.all(promises);
  }

  /**
   * Preloads the current user
   * @returns {Promise}
   */
  fetchCurrentUser() {
    return new Promise((resolve, reject) => {
      this.currentUser.fetch({
        success: resolve,
        error: reject
      });
    });
  }

  /**
   * Preloads the current account
   * @returns {Promise}
   */
  fetchCurrentAccount() {
    return new Promise((resolve, reject) => {
      this.currentAccount.fetch({
        success: resolve,
        error: reject
      });
    });
  }

  /**
   * Binds to our local event handlers
   * @returns {void}
   */
  bindEventHandlers() {
    this.on('start', this.onStartHandler.bind(this));
  }

  /**
   * EventHandler
   * Gets called when the application started
   * @param {object} options - Piped from Application.start
   * @returns {void}
   */
  onStartHandler(options) {
    // Render the base layout container
    this.rootLayout = new RootLayout(options);
    this.rootLayout.render();

    // Quick access to the content area
    this.contentArea = this.rootLayout.getRegion('content');

    // Lets make history
		Backbone.history.start({
      pushState: false,
      root: ''
    });

    // Bootstrap the header, will be the same for everything
    this.rootLayout.showChildView('header', new HeaderView());
  }
}
