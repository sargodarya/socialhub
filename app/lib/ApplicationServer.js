/**
 * The ApplicationServer is one of the core
 * components of the application. It sets up
 * the web server as well as all the middlewares
 * and passes requests to the AppDispatcher
 * which takes over the handling of all routes.
 */

'use strict';

const
  autoReap = require('multer-autoreap'),
  bodyParser = require('body-parser'),
  cookieParser = require('cookie-parser'),
  compress = require('compression'),
  express = require('express'),
  fs = require('fs'),
  glob = require('glob'),
  logger = require('morgan'),
  methodOverride = require('method-override'),
  mime = require('mime'),
  multer = require('multer'),
  path = require('path');

const Mailer = require('./Mailer');
const params = require('../../config/params');
const parsePort = require('./utils/parse-port');

class ApplicationServer {
  constructor(config) {
    /** @type {Object} **/
    this.app = express();

    /**
     * Holds all our environment agnostic configuration data
     * @type {Object}
     */
    this.config = config;

    /**
     * Contains all our controllers
     * @type {Object}
     */
    this.controllers = {};
  }

  /**
   * Sets up the whole express server.
   * NOTE: Order of execution matters
   * @returns {void}
   */
  init() {
    // short hand to save typing work
    let app = this.app;

    app.config = this.config;

    /** @type {Mailer} **/
    app.mailer = new Mailer(this.config);

    app.moveUploadedFile = this.moveUploadedFile;

    const env = process.env.NODE_ENV || 'development';
    app.locals.ENV = env;
    app.locals.ENV_DEVELOPMENT = env === 'development';

    app.set('views', this.config.root + '/app/views');
    app.set('view engine', 'jade');

    // Enables the express param transforms
    params(app);

    app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
      extended: true
    }));
    app.use(cookieParser());
    app.use(compress());
    app.use(express.static(this.config.root + '/public'));
    app.use(methodOverride());

    app.use(multer({
      dest: '/tmp/',
      limits: {
        fileSize: 1000000,
        files: 1
       }
    }).single('addon[image]'));

    // cleanup files using autoreap
    autoReap.options.reapOnError = true;
    app.use(autoReap);

    this.registerControllers();
    this.registerRoutes();
    this.registerErrorHandlers();

    // clean up remaining files if we have any
  }

  /**
   * Can be called before initialising the server
   * to inject additional middlewares
   * @param {function} middleware - middleware which should get injected
   * @returns {void}
   */
  injectMiddleware(middleware) {
    this.app.use(middleware);
  }

  /**
   * Loads all available controllers
   * @returns {void}
   */
  registerControllers() {
    const controllerFiles = glob.sync(this.config.root + '/app/controllers/*Controller.js');

    controllerFiles.forEach((controllerFile) => {
      let controllerName = controllerFile.split('/').pop().replace('Controller.js', '').toLowerCase();
      this.controllers[controllerName] = require(controllerFile);
    });
  }

  /**
   * Registers our routes
   * @returns {void}
   */
  registerRoutes() {
    const routeEntries = require(this.config.root + '/config/routes.js');
    for (let entryName in routeEntries) {
      let routeParts = entryName.split(' ');
      let targetUrl = routeParts[1];
      let requestMethod = routeParts[0].toLowerCase();
      let toAction = routeEntries[entryName].split('#');

      // If our target controller does not exist, throw an error
      let targetController = this.controllers[toAction[0].toLowerCase()];
      if (!targetController) {
        throw new Error('Controller ' + toAction[0] + ' does not exist.');
      }

      // If target action on the controller does not exist, throw an error
      let targetAction = targetController[toAction[1]];
      if (!targetAction) {
        throw new Error('Controller ' + toAction[0] + ' does not define action ' + toAction[1]);
      }

      // Finally register the route
      this.app[requestMethod](targetUrl, targetAction);
    }
  }

  /**
   * Registers our error handlers for 404 and 500 errors
   * @returns {void}
   */
  registerErrorHandlers() {
    // 404 handler
    this.app.use(function (req, res, next) {
      var err = new Error('Not Found');
      err.status = 404;
      next(err);
    });

    // If we're in development environment, show
    // the stack too for a 500 error.
    if(this.app.get('env') === 'development'){
      this.app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
          message: err.message,
          error: err,
          title: 'error'
        });
      });
    }

    // Catch all error. If 404 isn't thrown this
    // will throw a 500
    this.app.use(function (err, req, res, next) {
      res.status(err.status || 500);
        res.render('error', {
          message: err.message,
          error: {},
          title: 'error'
        });
    });
  }

  /**
   * Starts the server
   * @param {number} [listenPort] - pass to override config
   * @return {Promise}
   */
  listen(listenPort) {
    return new Promise((resolve, reject) => {
      // Port check for overriding
      const port = parsePort(listenPort) || this.config.port;

      if (port < 1024) {
        console.warn('Port ' + port + ' is a privileged port and thus express might refuse running');
      }

      // actually starts the express server
      this.app.listen(port, () => {
        this.onServerListening(port);
        resolve(port);
      });
    })
  }

  /**
   * Moves an uploaded file to target directory
   * @param {Object} file
   * @param {string} targetDirectory
   * @param {string} [fileName] - optional filename
   * @returns {Promise}
   */
  moveUploadedFile(file, targetPath, fileName) {
    return new Promise((resolve, reject) => {
      let targetFileName = (fileName || file.filename) + '.' + mime.extension(file.mimetype);
      let targetFilePath = path.join(targetPath, targetFileName);
      
      fs.rename(file.path, targetFilePath, function(err) {
        if (err) {
          return reject(err);
        }

        resolve(targetFileName, targetFilePath);
      });
    });
  }

  /**
   * Gets called when server is running successfully
   * @param {number} listeningPort - Port the server is running on
   * @returns {void}
   */
  onServerListening(listeningPort) {
    console.log('Express server listening on port ' + listeningPort);
  }
}

module.exports = ApplicationServer;
