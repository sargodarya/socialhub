import * as Backbone from 'backbone';

export class UserModel extends Backbone.Model {
  constructor(options) {
    super(options);
    this.url = '/users';
  }
}

export class UserCollection extends Backbone.Collection {
  constructor(options) {
    super(options);

    this.model = UserModel;
    this.url = '/users';
  }
}