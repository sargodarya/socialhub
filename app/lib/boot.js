'use strict';

// Small status code "enum"
const STATUS_CODES = {
  'ok': 0,
  'error': 1
};

// Import of our modules
const ApplicationServer = require('./ApplicationServer');
const config = require('../../config/config');
const DBConnection = require('./DBConnection');
const open = require('open');

/** @type {DBConnection} **/
const dbConnection = new DBConnection(config);

/** @type {ApplicationServer} **/
const appServer = new ApplicationServer(config);

/**
 * Generic error handler
 * @param {object} err
 * @returns {void}
 */
function errorHandler(err) {
  console.log(err.stack);
  process.exit(STATUS_CODES.error);
}

/**
 * Boots up the application and optionally injects some middlewares
 * @param {Array} injectMiddlewares
 * @returns {void}
 */
function boot(injectMiddlewares) {
  if (injectMiddlewares instanceof Array) {
    injectMiddlewares.forEach(middleware => appServer.injectMiddleware(middleware));
  }

  dbConnection.loadModels();
  appServer.init();

  // Booting up our database
  dbConnection
    .connect()
    .then(function() {
      appServer
        .listen()
        .then((port) => {
          open('http://localhost:' + port);
        });
    })
    .catch(errorHandler);
}

module.exports = boot;
