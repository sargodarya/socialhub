'use strict';

const nodemailer = require('nodemailer');
const fs = require('fs');
const path = require('path');
const jade = require('jade');

class Mailer {
  /**
   * Constructs a new mailer object
   *
   * @param {Object} config
   */
  constructor(config) {
    this.config = config;

    this.transporter = nodemailer.createTransport(this.config.emailTransport);

    this.templates = this.readTemplates(this.config.root + '/app/views/mails');
  }

  /**
   * Returns all compiled templates
   * @param {string} pathToTemplates
   * @returns {Object}
   */
  readTemplates(templatePath) {
    let templateFiles = fs.readdirSync(templatePath);
    let compiledTemplates = {};

    // Iterate through each file
    // and compile it with jade
    templateFiles.forEach(templateFile => {
      let templateName = templateFile.replace('.jade', '');
      compiledTemplates[templateName] = jade.compileFile(path.join(templatePath, templateFile));
    });

    return compiledTemplates;
  }

  /**
   * Sends a mail.
   *
   * A promise is returned but can safely be ignored. In
   * most cases you don't want to wait for a mail to be sent.
   *
   * @param {Object} options - needs to at least contain from, to, subject
   * @returns {Promise}
   */
  send(options) {
    return new Promise((resolve, reject) => {
      this.transporter.sendMail(options, function(err, info) {
        if (err) {
          console.log(err, info);
          return reject(err);
        }
        console.log('test');
        resolve(info);
      });
    });
  }

  /**
   * Send mail with template
   * @param {string} templateName - Name of the template to use
   * @param {Object} locals - Variables accessible from template
   * @param {Object} options - Meta data for the email
   * @returns {Promise}
   */
  sendWithTemplate(templateName, locals, options) {
    return new Promise((resolve, reject) => {
      if (!this.templates[templateName]) {
        let error = 'Email Template ' + templateName + ' does not exist.';
        console.log(error);
        return reject(error);
      }

      options.html = this.templates[templateName](locals);
      console.log(options.html);
      this.send(options)
        .then(resolve)
        .catch(reject);
    });
  }
}

module.exports = Mailer;