const config = require('./config');
const webpack = require('webpack');

const AUTOPREFIXER_BROWSERS = [
  'Chrome >= 20',
  'Firefox >= 24',
  'Explorer >= 8',
  'iOS >= 6',
  'Opera >= 12',
  'Safari >= 6'
];

const baseConfig = {
  context: config.root + '/app/frontend',
  entry: {
    app: ['./js/app', './sass/main.scss']
  },
  output: {
    path: config.root + '/public/js',
    filename: 'app.bundle.js',
    publicPath: config.root + '/public/js'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          cacheDirectory: true,
          presets: ['es2015']
        }
      },
      {
        test: /\.jade$/,
        loader: 'jade-loader',
      },
      {
        test: /\.scss$/,
        loader: 'style-loader!css-loader!postcss-loader!sass-loader'
      }
    ]
  },
  postcss: [
    require('cssnext')({ browsers: AUTOPREFIXER_BROWSERS }),
    require('postcss-nested')()
  ],
  plugins: []
};

const watchConfig = Object.assign({}, baseConfig, {
  debug: true
});

const buildConfig = Object.assign({}, baseConfig, {
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin({minimize: true})
  ]
});

module.exports = {
  watch: watchConfig,
  build: buildConfig
}