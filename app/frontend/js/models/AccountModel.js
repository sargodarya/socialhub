import Backbone from 'backbone';
import _ from 'lodash';

export const AccountModel = Backbone.Model.extend({
  idAttribute: '_id',

  urlRoot: '/accounts',

  initialize: function() {
    _.bindAll(this);
  },

  /**
   * Returns true if the addon is new
   * @param {AddonModel} addon
   * @returns {boolean}
   */
  isAddonNew: function(addon) {
    let isPremium = addon.get('isPremium');
    let addonAccessor = addon.get('accessor');
    let featureSet = isPremium ? 'premiumFeatures' : 'features';
    let features = this.get(featureSet);

    return typeof features[addonAccessor] === 'undefined' &&
      !window.App.collections.AddonCollection.hasAddonBeenSeen(addonAccessor);
  },

  /**
   * Returns true if given addon is enabled
   * @param {AddonModel} addon
   * @returns {boolean}
   */
  isAddonEnabled: function(addon) {
    let features = addon.get('isPremium') ? this.get('premiumFeatures') : this.get('features');
    return !!features[addon.get('accessor')];
  },

  /**
   * returns the number of new addons
   * @return {number}
   */
  getNewAddonCount: function() {
    return window.App.collections.AddonCollection.models
      .filter(addon => this.isAddonNew(addon)).length;
  },

  /**
   * Updates the model with the given feature
   * @param {AddonModel} addon
   * @returns {boolean}
   */
  toggleAddon: function(addon) {
    return new Promise((resolve, reject) => {
      let isPremium = addon.get('isPremium');
      let addonAccessor = addon.get('accessor');
      let featureSet = isPremium ? 'premiumFeatures' : 'features';
      let previousFeatures = this.get(featureSet);

      // create copy of object
      let newFeatures = Object.assign({}, previousFeatures);

      if (typeof previousFeatures[addonAccessor] !== 'undefined') {
        // classical toggle of true <=> false
        newFeatures[addonAccessor] = !previousFeatures[addonAccessor];
      } else {
        newFeatures[addonAccessor] = true;
      }

      this.set(featureSet, newFeatures);
      this.save(null, {
        success: (model, response) => {
          resolve();
        },
        error: (model, response, error) => {
          this.set(featureSet, previousFeatures);
          reject(error);
        },
        wait: true
      });
    });
  }
});