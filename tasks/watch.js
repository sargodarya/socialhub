const path = require('path');
const webpack = require('webpack');
const config = require(path.join(__dirname, '../config/config.js'));
const webpackConfig = require(config.root + '/config/webpack.config.js').watch;
const webpackDevMiddleware = require('webpack-dev-middleware');

const BootApplication = require('../app/lib/boot');

// Setup the dev Server
webpackConfig.output.path = '/';

// Setup the compiler
const compiler = webpack(webpackConfig);

// Boots the application
BootApplication([webpackDevMiddleware(compiler, {
  publicPath: '/js/'
})]);