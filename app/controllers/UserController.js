'use strict';

class UserController {
  /**
   * "Should" return the currently logged in user
   * NOTE: "Should" because right now we don't have a login so
   * we always return the only user in our db at the moment.
   *
   * This might be solved with simply using passport
   */
  static getCurrentUser(req, res) {
    // Only return what's necessary
    let requestFields = [
      '_id',
      'userName',
      'email'
    ].join(' ');

    User
      .findOne({}, requestFields)
      .exec(function(err, user) {
        if (err) {
          res.status(500).send(err);
          return res.end;
        }

        res.send(user);
        res.end();
      });
  }
}

module.exports = UserController;
