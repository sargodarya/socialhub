import * as Backbone from 'backbone';

export class AddonModel extends Backbone.Model {
  constructor(options) {
    super(options);
    this.url = '/addons';
  }
}

export const AddonCollection = Backbone.Collection.extend({
  model: AddonModel,
  
  storageKey: 'addons',
  
  url: '/addons',
  
  initialize: function(models, options) {
    this.storage = {
      'seen': [],
      'pending': []
    };

    // Try to load the seen addons from localStorage
    // wrapped in try catch because it might fail
    // if the user is using private mode.
    try {
      let storage = localStorage.getItem(this.storageKey);
      if (!storage) {
        localStorage.setItem(JSON.stringify(this.storage));
      } else {
        this.storage = JSON.parse(storage);
      }
    } catch (e) {}
  },

  /**
   * Returns true if the addon has been seen before
   * @param {string} addonAccessor - The addons accessor
   * @returns {bool} - true if it has been seen before
   */
  hasAddonBeenSeen: function(addonAccessor) {
    return this.storage.seen.indexOf(addonAccessor) > -1;
  },

  /**
   * Returns true if the addon is pending
   * @param {string} addonAccessor
   * @returns {boolean}
   */
  isAddonPending: function(addonAccessor) {
    return this.storage.pending.indexOf(addonAccessor) > -1;
  },

  /**
   * Marks an addon as seen and persists the change
   * in local storage if it hasn't been seen before
   * @param {string} addonAccessor - The addons accessor
   * @returns {bool} - true if it hasn't been seen before
   */
  markAddonAsSeen: function(addonAccessor) {
    if (!this.hasAddonBeenSeen(addonAccessor)) {
      this.storage.seen.push(addonAccessor);
      this.persistLocal();
      this.trigger('addon:markedAsRead');
      return true;
    }
    return false;
  },

  /**
   * Marks an addon as pending and persists the change in
   * local storage if it wasn't pending before.
   * @param {string} addonAccessor
   * @returns {boolean}
   */
  markAddonAsPending: function(addonAccessor) {
    if (!this.isAddonPending(addonAccessor)) {
      this.storage.pending.push(addonAccessor);
      this.persistLocal();
      this.trigger('addon:markedAsPending');
      return true;
    }
    return false;
  },

  /**
   * Persists the collections storage locally
   * @returns {void}
   */
  persistLocal: function() {
    localStorage.setItem(this.storageKey, JSON.stringify(this.storage));
  }
}, {
  collectionName: 'AddonCollection'
})
