'use strict';

const _ = require('lodash');

class AccountController {

  /**
   * PUT method is usually used to update a whole
   * object but we're applying a partial logic in here
   * only allowing updates to be made to the features
   * property of the account. This should normally be
   * a request to a different route.
   *
   * In case the user tried to enable premiumFeatures we're
   * notifiying the sales rep and send back a 401.
   *
   * Otherwise we send a 200 back.
   */
  static update(req, res) {
    // NOTE: normally we should validate here that the given
    // account id belongs to our current user and the user
    // has the rights to modify this. Leaving this out
    // means everyone could just happily change everything for
    // no good.
    //
    // I'm skipping this for the sake of this example,
    // just wanted to leave this as a notice that I thought
    // of it.

    // Validate that we have our feature objects
    // otherwise send bad request status
    if (!req.body.premiumFeatures || !req.body.features) {
      return res.status(400).end();
    }

    // Checks if we have any new premium features enabled
    let newlyEnabledPremiumFeatures = [];
    for (let featureAccessor in req.body.premiumFeatures) {
      // checks that feature is enabled in request
      // then checks that stored state is not enabled
      if (req.body.premiumFeatures[featureAccessor] &&
          !!req.account.premiumFeatures[featureAccessor] === false) {
        newlyEnabledPremiumFeatures.push(featureAccessor);
      }
    }

    // if we have newly enabled premium features
    // we DO NOT enable them. These can only be enabled by
    // Customer support.
    // In this case send an email and a 401 Unauthorized HTTP
    // Status
    if (newlyEnabledPremiumFeatures.length > 0) {

      // Normally the user should be stored in the request
      // for this we take the only user in this example...
      // We also ignore that the request is done at this point already
      // because a mail shouldn't defer the response
      User.findOne({}, function(err, user) {
        if (err) {
          return console.log(err);
        }

        let mailLocals = {
          features: newlyEnabledPremiumFeatures,
          account: req.account,
          user: user
        };

        req.app.mailer.sendWithTemplate('addon_request', mailLocals, {
          from: 'sargodarya@gmail.com',
          to: req.app.config.salesEmail,
          subject: 'Addon Request for Account ' + user.accountId
        });
      });

      return res
        .status(401)
        .send('Thank you for your interest in this premium addon. A customer sales rep will get in touch with you soon to discuss details.')
        .end();
    }

    // Defer updating to the model
    req.account
      .updateEnabledAddons(req.body.features)
      .then(function(features) {
        res.send(features);
        res.end();
      })
      .catch(function(err) {
        // This part actually shouldn't happen. If it does
        // we have an internal server error.
        res.status(500).end();
      });
  }

  /**
   * "Should" return the account associated with the
   * currently logged in user
   * NOTE: "Should" because right now we don't have a login so
   * we always return the only account in our db at the moment.
   *
   * This might be solved with simply using passport
   */
  static getCurrentAccount(req, res) {
    // Only request what's necessary for this task
    let requestFields = [
      '_id',
      'features',
      'hubVersion',
      'locale',
      'name',
      'premiumFeatures'
    ].join(' ');

    Account
      .findOne({}, requestFields)
      .exec(function(err, account) {
        if (err) {
          res.status(500).send(err);
          return res.end;
        }

        res.send(account);
        res.end();
      });
  }
}

module.exports = AccountController;
