import Marionette from 'backbone.marionette';
import _ from 'lodash';

class HeaderView extends Marionette.LayoutView {
  constructor(options) {
    super(options);
    this.template = require('./HeaderView.jade');
    this.app = window.App;

    _.bindAll(this);

    // Bind to event
    this.listenTo(this.app.collections.AddonCollection, 'addon:markedAsRead', _.throttle(this.render))
  }

  templateHelpers() {
    return {
      'getNewAddonCount': this.app.currentAccount.getNewAddonCount,
      'userName': this.app.currentUser.get('userName')
    };
  }

  /**
   * Gets rid of the wrapping div
   * @returns {void}
   */
  onRender() {
    this.$el = this.$el.children();
    this.$el.unwrap();
    this.setElement(this.$el);
  }
}

export default HeaderView;