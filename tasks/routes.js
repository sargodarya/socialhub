'use strict';

const routes = require(__dirname + '/../config/routes');

console.log('Listing all routes:');

for (let routeTarget in routes) {
  let action = routes[routeTarget];
  let routeParts = routeTarget.split(' ');

  console.log(routeParts[0].toUpperCase() + ' ' + routeParts[1] + ' => ' + action);
}