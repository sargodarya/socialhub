# SocialHub Addons

This repository contains the sample code for the addon functionality for SocialHub.

## Requirements

- node

- npm

- mongodb

## Getting started

    $ git clone git@bitbucket.org:sargodarya/socialhub.git socialhub && cd socialhub
    $ npm install
    $ npm run migrate
    $ npm run build
    $ EMAILPASS=YOURPASS npm run start

NOTE: NodeMailer is currently only configured to send mails using
the gmail transport. If you want to use a different one or configure
it differently you have to adjust transport in config/config.js

## Running the development server

The development server can be started using ```npm run watch```.

This starts the application with the webpack dev middleware and
rebuilds the app.bundle.js everytime something in the frontend changes.

**NOTE:** The webpack dev middleware shadows a built app.bundle.js and
stores the changed app.bundle.js in the memory so you won't see any
changes in that file unless you run an build.

## Trigger a build

You can start a build with ```npm run build```.

A new build will be triggered using webpack and an optimized and
uglified (read: minified) app.bundle.js will be placed in public/js/.

## Additional Documentation

[Implementations](./docs/implementations.md)

[Project Structure](./docs/project-structure.md)

[Technology Stack](./docs/technology-stack.md)

## Available NPM Tasks

| task        | description                             |
|-------------|-----------------------------------------|
| **build**   | builds a production version with webpack|
| **watch**   | runs the development server via webpack |
| **start**   | starts the express backend              |
| **migrate** | migrates the database                   |
| **routes**  | lists all available routes associations |

## Things which should/could be improved

Following is a list of things which I consider important but are
outside of the spec or scope of this project.

### Extract sensitive data into a file which is not tracked in VC

There is never a good reason to store sensitive data like passwords,
private keys or API keys in version control.

For this, you usually should create some kind of .rc file from which
the configuration gets read.

### /accounts/me to get the current account and /users/me to get the current user

At the moment, GET /accounts and GET /user return a single user
and account to be consistent with the spec but this feels
very wrong as these routes should normally return an index
of those collections.

### Passport for sessions

Right now the code is always using the only records available
in the account and user collection for the sake of this example.

Normally, something like Passport should be used to facialite logins.

### Script Versioning and long caching

Currently the built file is requested every time the app loads.
Considering that it might become a very large file as it is a
single page application, having this sent each time is bad.
This could be countered by inserting a content hash into the file
and giving the file a relatively long caching time.

If something should change, the content hash will change and
the new script will be loaded.

### NGINX as static asset server

To speed up loading of assets and to ease up the load on
our node process it makes sense to handle static assets
via NGINX.

### AWS buckets for uploads

Image uploading might be optimized by uploading to an AWS
bucket and delivering the images via them.

### Enable source maps for production

Source maps are not implemented for production at the moment
and this really is a matter of preference. Correctly
configured, sourcemaps can be accessible only from an internal
network so no one else can look at the clean code.

### Use a logging library like winston

Outputting logs to the console actually slows down an app
considerably [in the browser](http://stackoverflow.com/questions/11426185/will-console-log-reduce-java-script-execution-performance)
as well as in node.js as it is synchronous and as such, blocking.

### Provision everything using Vagrant, Ansible or dokku

It makes a lot of sense to have an application which one can
easily deploy without a lot of hassle and needing to take
care of services. For development purposes, Vagrant is an
excellent choice allowing us to set up an environment
without cluttering up the developers machine.

Ansible provides the hardcore configuration to scale everything
up easily in the cloud.
