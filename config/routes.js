/**
 * This file contains all route mappings
 * to controllers with the default resource
 * routes being last.
 *
 * For the Url, you can use substitutions which are then passed
 * into the controller as parameters.
 */
'use strict';

var i = require('inflection');

// Our base routes
let routes = {
  'get /':                 'Index#index',
  'get /users':            'User#getCurrentUser',
  'get /accounts':         'Account#getCurrentAccount',
  'put /accounts/:account':     'Account#update'
};

/**
 * Automatically creates RESTful resource paths
 * @param {string} controllerName - Controller to RESTify
 * @returns {void}
 */
function resource(controllerName) {
  let newRoutes = {};
  let basePath = '/' + i.pluralize(controllerName.toLowerCase());
  let idPath = '/:' + controllerName.toLowerCase();

  newRoutes['get ' + basePath]                    = controllerName + '#index';
  newRoutes['post ' + basePath]                   = controllerName + '#create';
  newRoutes['get ' + basePath + '/new']           = controllerName + '#new';
  newRoutes['get ' + basePath + idPath]           = controllerName + '#show';
  newRoutes['put ' + basePath + idPath]           = controllerName + '#update';
  newRoutes['delete ' + basePath + idPath]        = controllerName + '#destroy';
  newRoutes['get ' + basePath + idPath + '/edit'] = controllerName + '#edit';

  Object.assign(routes, newRoutes);
}

// Resourceful routing for AddOns
resource('Addon');

module.exports = routes;
