'use strict';

const mongoose = require('mongoose');
const _ = require('lodash');

const AccountSchema = new mongoose.Schema({
  appId : String,
  createdTime : Date,
  expirationTime : Date,
  features : {
    type: Object,
    default: {
  	  approvalProcess : false,
  	  categories : false,
  	  categoriesForceUserToTag : false,
  	  cp_beta : false,
  	  coworking : false,
  	  insightsUserActions : false,
  	  templates : false
    }
	},
  history : {
    type: Array,
    default: [
      {
        type: "enabled",
        time : Date
      }
    ],
  },
  hubVersion : String,
  locale : {
    type: String,
    default: 'de'
  },
  name : String,
  package : String,
  premiumFeatures : {
    type: Object,
    default: {
      security : false
    }
  },
  realtimeAppId : String,
  tags : Array
});

/**
 * Updates the models features with given feature object
 * @param {Object} features - Contains Addon.accessor:bool
 * @returns {Promise}
 */
function updateEnabledAddons(features) {
  return new Promise((resolve, reject) => {
    Addon.find({}, 'accessor', (err, addons) => {
      // This is a sanity check so the user is not able
      // to enable premium addon as part of a normal
      // available addon
      let availableFeatures = addons.map(addon => addon.accessor);
      let featuresToUpdate = _.pick(features, availableFeatures);

      // Iterate through features and convert to boolean
      for (let featureName in featuresToUpdate) {
        this.features[featureName] = !!featuresToUpdate[featureName];
      }

      // Save the account changes
      this.markModified('features');
      this.save((err) => {
        if (err) {
          return reject(err);
        }

        // Resolve promise with our current account
        resolve(this);
      });
    });
  });
}

// Assign defined methods to the account schema
AccountSchema.methods.updateEnabledAddons = updateEnabledAddons;

module.exports = mongoose.model('Account', AccountSchema);
