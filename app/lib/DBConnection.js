'use strict';

const
  glob = require('glob'),
  mongoose = require('mongoose');

/**
 * This is a small wrapper around mongoose
 * to abstract away the nitty gritty details
 * about DB handling so one might exchange
 * the DB with something else. Usually you
 * would separate that into a db adapter.
 */
class DBConnection {
  constructor(config) {
    /**
     * Holds an instance of the mongoose connection
     * @type {mongoose.connection}
     */
    this.connection = null;

    /** @type {object} **/
    this.config = config;
  }

  /**
   * Binds all event handlers which can be fired from
   * @returns {void}
   */
  bindEventHandlers() {
    this.connection.on('error', this.onErrorHandler.bind(this));
  }

  /**
   * Opens the connection to the server.
   * @returns {Promise} - Resolved on successful connection
   */
  connect() {
    return new Promise((resolve, reject) => {
      mongoose.connect(this.config.db);
      this.connection = mongoose.connection;

      this.bindEventHandlers();
      this.connection.once('open', resolve);
      this.connection.once('error', reject);
    });
  }

  /**
   * Sets up all our models and puts them into nodes global scope
   * to be accessible everywhere.
   *
   * NOTE: Putting things in global scope is usually bad but seems to be
   *       common in almost every MVC framework for some reason.
   *
   * @returns {void}
   */
  loadModels() {
    const modelFiles = glob.sync(this.config.root + '/app/models/*.js');
    modelFiles.forEach((modelFile) => {
      let model = require(modelFile);
      let modelName = model.modelName;
      GLOBAL[modelName] = model;
      console.info('Mongoose: Registered model \'' + modelName + '\' on the global scope.');
    });
  }

  /**
   * EventHandler#onError
   * Gets called in case the connection errors out
   * @param {ErrorEvent} errorEvent
   * @returns {void}
   */
  onErrorHandler(errorEvent) {
    console.log(errorEvent);
  }
}

module.exports = DBConnection;
