# Technology Stack

## Backend

### Express

As condition from the spec, we're using express.js and
various middlewares to facialiate request handling. For this
a small wrapper has been written so routes can be specified
in an MVC manner in the config/routes.js file.

### Mongoose / MongoDB

Mongooses model handling makes everything a lot easier and
comfortable so I settled with Mongoose as ODM. The model
files for Mongoose are living in app/models.

### Multer + Multer-AutoReap

For multipart/form-data (eg. the image upload) we use multer
and autoreap as plugin. Files are first uploaded to /tmp/ and then
moved in the appropriate place. If files remain, the will be either
cleared by autoreap or deleted by the system.

### Jade

Jade is used as a template engine throughout the project because it
dramatically reduces the amount of characters you have to write, is
easy to use and the mixin functionality is just plain wonderful.

### NodeMailer

NodeMailer is the easiest to use solution for Node.js providing
various transports to satisfy ones needs. For this project,
I wrote a small wrapper around it which allows me to send predefined
templates sitting in app/views/mails. The configuration for the
mail transport can be found in config/config.js.

### lodash

The slightly smaller underscore library is used for certain
functionalities like plucking properties.

### Webpack

This handles compilation, concatenation, transpiling and building
our frontend code into a nice little neat package. The configuration
file for this can be found in app/webpack.config.js and is split
into a watchConfig and buildConfig which are used for those tasks.

## Frontend

### Marionette

I've spent a lot of time reading into Marionette the last few
days and optimizing code the more I've read about it. Marionette is
used as our View and Controller layer and also handles routing.

I find the fact that it is considered best practice to have
a global app object a little bit off putting though and consider
this very bad for isolated testing purposes.

### Backbone

Backbone is of course used by Marionette but we only use it
for it's model and collection functionality.

### SASS / BEM

For styles we're using SASS and BEM. This allows us to have
nice structured styles while keeping them in a maintainable
state.

### Jade

Already used in the backend, it only makes sense to also use
this in the frontend.

### Babel / ES6

ES6 is slowly coming and with transpilers like babel there is
kind of no excuse to not using it already. I tried to use ES6
where possible in the code, this might be optimized though.

### PostCSS / Autoprefixer

If you're going to write modern CSS you usually have to include
lots of vendor prefixes. PostCSS/Autoprefixer goes through
the CSS code and does that automatically for all given browsers
so you can focus on writing good, clean CSS and autoprefixer
does all the rest.