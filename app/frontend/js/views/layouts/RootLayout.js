import * as Marionette from 'backbone.marionette';
import * as _ from 'lodash';

class RootLayout extends Marionette.LayoutView {
  constructor(...rest) {
    super(...rest);
    this.template = require('./RootLayout.jade');
  }

  regions() {
    return {
      header: '.header__container',
      content: '.content__container'
    }
  }
}

export default RootLayout;