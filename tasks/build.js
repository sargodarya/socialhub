const path = require('path');
const webpack = require('webpack');
const config = require(path.join(__dirname, '../config/config.js'));
const webpackConfig = require(config.root + '/config/webpack.config.js');

// Setup the compiler
console.log('Setting up webpack.');
const compiler = webpack(webpackConfig.build);

console.log('Starting build...');
const startTime = +new Date();
compiler.run(function(err, stats) {
  const timeTaken = (+new Date() - startTime) / 1000;

  if (err) {
    console.log(err);
    console.log('Build failed after')
  }

  console.log('Build finished after ' + timeTaken + 's');
});