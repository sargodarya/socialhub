import Marionette from 'backbone.marionette';
import _ from 'lodash';

const HomeView = Marionette.ItemView.extend({
  tagName: 'div',
  className: 'home',
  template: require('./HomeView.jade')
});

export default HomeView;