import Marionette from 'backbone.marionette';
import _ from 'lodash';

const LoadingView = Marionette.ItemView.extend({
  tagName: 'div',
  className: 'loading',
  template: _.template('<span>Loading</span>')
});

export default LoadingView;