# Stories & Implementations

## #1 As an admin I want to be able to trigger a notification for new addons

A notification is defined as a bubble popping up next to the
addon menu entry for this project and additionally a notification,
using the HTML5 Notifications API or an inline notification as
fallback if we don't have permissions for the Notifications API.

I'm distinguishing notifications into two parts in this case:

- realtime notifications

- reload notification

Reload notifications are taken care of by story #2, this
story will take care of the real-time notifications.

Real-time notifications will be sent to the user when a given
button for the addon is pressed. The flow is as following:

1. Administrator clicks notify button on the addon page OR
enables notify on creation of a new addon

2. Post request is sent to the backend (eg. /addons/:id/notify)

3. Backend broadcasts a message to all connected clients

4. Frontend receives the message

5. Frontend checks if the given addon is already enabled
(might be possible)

  - If it is enabled, skip the rest
  
6. Push the new addon into the AddonCollection

7. Increase the notification counter

8. Check if permission is granted for HTML5 notifications.

  - If it is enabled, show HTML5 notification
  
  - If it is not enabled, show inline browser notification

Additionally, we can take this a step further and display
links like **Show me what's new** or **Remind me later**.

In case the user clicks on Remind me later, we store his
choice in localStorage and show a notification the next
time he reloads or logs in.

If he clicks on **Show me what's new** he will be redirected
to the AddonCollectionView and the item will be marked no longer
as new on the next page reload. (see Story #2)

## #2 As a user I want to be notified if a new addon is released

For this one first needs to consider what a new addon is to the user.
For the sake of this project, the definition is as follows:

_An addon counts as new if the given account does not have the
addons accessor in either his feature or premiumFeatures as key
and if the addons ItemView hasn't been rendered before._

To check if the ItemView of the addon has been rendered before,
we're storing the addons accessor in localStorage.

## #3 As a user I want to be able to view a list of currently available addons​

This functionality has been implemented using the ```settings/addon``` route
in the router. Once clicked, it will show a loading view
and once the addon collection finished updating, show a
CollectionView of all addons.

Addons can be created by going to /addons/new. Images are
being uploaded to public/uploads.

## #4 As a user I want to be able to de­/activate addons.

An addon can be toggled by using the toggle button on
the right side of an addon. This triggers a call to
Account.toggleAddon which toggles the addon state and
sends a put request to /account/:accountId.

The backend then checks that account.features only
contains available features and updates them accordingly.

(a.) If the user tries to enable a premium addon, an email is
sent to the email address specificed in config/config.js salesEmail
and (b.) a 401 Unauthorized error is sent back with the appropriate
error message which is shown just below the addons name.
