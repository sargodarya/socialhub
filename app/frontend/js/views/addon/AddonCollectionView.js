import * as Marionette from 'backbone.marionette';
import * as _ from 'lodash';

import AddonItemView from './AddonItemView';
import AddonNoItemView from './AddonNoItemView';

const AddonCollectionView = Marionette.CollectionView.extend({
  tagName: 'div',
  className: 'addons',
  childView: AddonItemView,
  emptyView: AddonNoItemView
});

export default AddonCollectionView;