var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'socialhub'
    },
    port: 1337,
    db: 'mongodb://localhost/socialhub-development',
    salesEmail: 'addons-test@mailinator.com',
    emailTransport: {
      service: 'Gmail',
      auth: {
        user: 'sargodarya@gmail.com',
        pass: process.env.EMAILPASS
      }
    }
  },

  test: {
    root: rootPath,
    app: {
      name: 'socialhub'
    },
    port: 1337,
    db: 'mongodb://localhost/socialhub-test',
    salesEmail: 'addons-test@mailinator.com',
    emailTransport: {
      service: 'Gmail',
      auth: {
        user: 'sargodarya@gmail.com',
        pass: process.env.EMAILPASS
      }
    }
  },

  production: {
    root: rootPath,
    app: {
      name: 'socialhub'
    },
    port: 1337,
    db: 'mongodb://localhost/socialhub-production',
    salesEmail: 'addons-test@mailinator.com',
    emailTransport: {
      service: 'Gmail',
      auth: {
        user: 'sargodarya@gmail.com',
        pass: process.env.EMAILPASS
      }
    }
  }
};

module.exports = config[env];
