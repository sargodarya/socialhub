
import AddonCollectionView from '../views/addon/AddonCollectionView';
import HomeView from '../views/home/HomeView';
import LoadingView from '../views/loading/LoadingView';

export default class ApplicationController {

  /**
   * Returns a new instance of the ApplicationController.
   * As we need to work with events and want to access
   * global object we pass the app instance in here and
   * store it.
   *
   * @param {Application} app
   * @returns {ApplicationController}
   * @constructor
   */
  constructor(app) {
    /** @type {Application} **/
    this.app = app;
  }

  /**
   * This is our base route to fall back when nothing has been selected
   */
  index() {
    this.app.contentArea.show(new HomeView);
  }

  /**
   * This is the addon page controller
   */
  addonList() {
    // Show loading screen
    this.app.contentArea.show(new LoadingView);

    // Update collection before showing the
    // actual view.
    this.app.collections.AddonCollection.fetch({
      success: () => {
        /** @type {AddonCollectionView} **/
        let addonCollectionView = new AddonCollectionView({
          collection: this.app.collections.AddonCollection
        });

        this.app.contentArea.show(addonCollectionView);
      },
      error: () => {
        // TODO: Add an error view
      }
    });
  }

  /**
   * Returns the routes defined by this controller
   * @returns {Object}
   */
  static routes() {
    return {
      '': 'index',
      'home': 'index',
      'settings/addons': 'addonList'
    }
  }
}