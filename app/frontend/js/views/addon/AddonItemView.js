import Marionette from 'backbone.marionette';
import _ from 'lodash';

const AddonItemView = Marionette.ItemView.extend({
  className: 'addon',

  initialize: function() {
    this.app = window.App;

    // Bind all methods to this context
    _.bindAll(this);
  },

  template: require('./templates/AddonItemView.jade'),

  templateHelpers: function() {
    return {
      'isAddonEnabled': this.isAddonEnabled,
      'isAddonNew': this.isAddonNew,
      'isAddonPending': this.isAddonPending
    }
  },

  ui: {
    addonToggle: '.switch__toggle'
  },

  triggers: {
    'click @ui.addonToggle': 'toggle:addon'
  },

  /**
   * Toggles this addon and rerenders the
   * view in case it worked.
   *
   * If the server returns an error, check for the
   * status code and show the error.
   *
   * @returns {void}
   */
  onToggleAddon: function() {
    this.app.currentAccount
      .toggleAddon(this.model)
      .then(this.render)
      .catch((err) => {
        switch (err.xhr.status) {
          case 401:
            this.model.set('error', err.xhr.responseText);
            this.render();
            this.app.collections.AddonCollection.markAddonAsPending(this.model.get('accessor'));
            break;

          default:
            this.model.set('error', 'Something unexpected just happened. Please try again later.');
            this.render();
        }
      });
  },

  /**
   * Returns true if the addon is new
   * @returns {boolean}
   */
  isAddonNew: function() {
    return this.app.currentAccount.isAddonNew(this.model);
  },

  /**
   * Toggles the given addon for the current user
   * @returns {boolean}
   */
  isAddonEnabled: function() {
    return this.app.currentAccount.isAddonEnabled(this.model);
  },

  /**
   * Returns true if the addon is pending and
   * waiting for a sales rep to be enabled
   * @returns {boolean}
   */
  isAddonPending: function() {
    return this.app.collections.AddonCollection.isAddonPending(this.model.get('accessor'));
  },

  /**
   * Marks addon as seen
   * @returns {void}
   */
  onRender: function() {
    this.app.collections.AddonCollection.markAddonAsSeen(this.model.get('accessor'));
  }

});

export default AddonItemView;